output "region" {
    value       = local.region
    description = "The region deployed to."
}

output "template_bucket" {
    value       = local.template_bucket
    description = "The name of the bucket that the CloudFormation stack was uploaded to."
}

output "stack_name" {
    value       = local.stack_name
    description = "A unique identifier of the stack."
}

output "outputs" {
    value       = aws_cloudformation_stack.module.outputs
    description = "A map of outputs from the stack."
}
