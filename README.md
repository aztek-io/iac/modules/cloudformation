# Module: cloudformation

Creates an CloudFormation stack in AWS.

## Example Usage

How to create a CloudFormation stack using this module:

```hcl
module "my_stack" {
    source          = "git@gitlab.com:aztek-io/iac/modules/cloudformation.git?ref=v0.3.0"
    template_path   = "${path.module}/my_stack.yml"
}
```

## Argument Reference

The following arguments are supported:

### Required Attributes

* `template_path`   - (Required|string) The file path of the CloudFormation template.

### Optional Attributes

* `capabilities`    - (Optional|string) A list of capabilities. Valid values: `CAPABILITY_IAM`, `CAPABILITY_NAMED_IAM`, or `CAPABILITY_AUTO_EXPAND` (Defaults to an empty list)
* `parameters`      - (Optional|map(string)) The parameters passed to the CloudFormation stack (Defaults to an empty map).
* `region`          - (Optional|string) The name of the AWS region to deploy to (Defaults to `us-west-2`)
* `stack_name`      - (Optional|string) The name of the CloudFormation stack (Defaults to a random 64 character string).
* `template_bucket` - (Optional|string) The name of the bucket to upload the template to (Defaults to an empty string which will result in a bucket being created)

## Attribute Reference

* `region`          - The region deployed to.
* `template_bucket` - The name of the bucket that the CloudFormation stack was uploaded to.
* `stack_name`      - A unique identifier of the stack.
* `outputs`         - A map of outputs from the stack.

