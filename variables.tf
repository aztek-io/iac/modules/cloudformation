variable "region" {
    default = "us-west-2"
}

variable "parameters" {
    type    = map(string)
    default = {}
}

variable "capabilities" {
    type    = list(string)
    default = []
}

variable "stack_name" {
    default = ""
}

variable "template_bucket" {
    default = ""
}

variable "template_path" {}
