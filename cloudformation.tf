resource "aws_cloudformation_stack" "module" {
    name            = local.stack_name
    parameters      = var.parameters
    capabilities    = var.capabilities
    template_url    = "https://s3-${local.region}.amazonaws.com/${local.template_bucket}/${aws_s3_bucket_object.module.id}"
}
