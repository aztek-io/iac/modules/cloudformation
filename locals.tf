resource "random_string" "stack_name" {
    length  = 61
    special = false
}

resource "random_string" "bucket_name" {
    length  = 32
    upper   = false
    special = false
}

locals {
    template_bucket     = length(var.template_bucket) > 0 ? var.template_bucket : aws_s3_bucket.module[0].id
    region              = data.aws_region.current.name
    stack_name          = length(var.stack_name) > 0 ? var.stack_name : join("-", ["tf", random_string.stack_name.result])
    template_path_array = split("/", var.template_path)
    template_dir_count  = length(local.template_path_array)
    template_filename   = join("-", [filemd5(var.template_path), element(local.template_path_array, local.template_dir_count - 1)])
}
