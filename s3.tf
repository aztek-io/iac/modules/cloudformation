resource "aws_s3_bucket" "module" {
    count   = length(var.template_bucket) > 0 ? 0 : 1
    bucket  = random_string.bucket_name.result
}

resource "aws_s3_bucket_object" "module" {
    bucket  = local.template_bucket
    key     = local.template_filename
    source  = var.template_path
    etag    = filemd5(var.template_path) # needed to ensure reupload of file after changes to the local copy.
}
